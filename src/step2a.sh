#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Pass pattern as argument"
    exit 1
fi

root=$(pwd)
queue_dir="$root/data/queue"
done_dir="$root/data/done"
hg_dir="$root/data/hg"
git_dir="$root/data/git"

batch_size=5
index=1

pattern=$1

for file in $queue_dir/*
do
    if [[ -f $file ]]; then
        repo_slug=$(basename $file)

        if [[ ! $repo_slug =~ $pattern ]]; then
          continue
        fi

        cd $git_dir

        echo "=== Clone $repo_slug ==="
        git clone ssh://git@bitbucket.org/studioibizz/$repo_slug

        if [[ ! -d $repo_slug ]]; then
            echo "Cloning of repo is failed"
            continue
        fi

        #repo_slug="viewmaker-website-v1"
        cd $repo_slug

        OLDIFS=$IFS
        IFS=$'\n'
        for branch in `git branch -a | cut -c 3-`
        do
          branch=${branch/remotes\/origin\//}

#          if [[ $branch == "production" ]]
#          then
#            continue
#          fi

          if [[ $branch == HEAD* ]]
          then
            continue
          fi

          if [[ $branch =~ ":" || $branch == "(inactive)" ]]
          then
            continue
          fi

          echo -e "\tUpdate to branch $branch"
          git checkout -f -q $branch
          continue
          actualbranch=$(hg branch)
          newbranch="${actualbranch// /-}"
          newbranch="${newbranch//#/}"
          echo -e "\tReplace $actualbranch to $newbranch"
          echo -e "\tCreate bookmark 'git-$newbranch'"
          hg bookmark "git-$newbranch"
        done
        IFS=$OLDIFS
    fi

    index=$(($index+1))
done