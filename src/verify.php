<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function dummy($array) { return $array; }

//$cache = array(
//    'hg' => array(
//        0 => dummy(array('name' => 'Tesselschade / Website', 'scm' => 'Tesselschade / Website',)),
//        1 => dummy(array('name' => 'Met Zorg / Website', 'scm' => 'Met Zorg / Website',)),
//        2 => dummy(array('name' => 'Fityx / Ergotoponline', 'scm' => 'Fityx / Ergotoponline',)),
//        3 => dummy(array(
//            'name' => 'Countryhouse de Vlasschure / Website',
//            'scm' => 'Countryhouse de Vlasschure / Website',
//        )),
//        4 => dummy(array('name' => 'Watchuseek / Website', 'scm' => 'Watchuseek / Website',)),
//        5 => dummy(array(
//            'name' => 'Jobse & Partners / Corporate Website',
//            'scm' => 'Jobse & Partners / Corporate Website',
//        )),
//        6 => dummy(array('name' => 'Autorola / A12 Automarkt', 'scm' => 'Autorola / A12 Automarkt',)),
//        7 => dummy(array(
//            'name' => 'Autorola / Autoverkoopservice',
//            'scm' => 'Autorola / Autoverkoopservice',
//        )),
//        8 => dummy(array(
//            'name' => 'Autorola / Motorverkoopservice',
//            'scm' => 'Autorola / Motorverkoopservice',
//        )),
//        9 => dummy(array('name' => 'Cito / De derde generatie', 'scm' => 'Cito / De derde generatie',)),
//    ),
//);
//
//var_dump($cache);
//exit;


require_once dirname(__FILE__) . '/configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/includes/bitbucket.inc.php';
require_once dirname(__FILE__) . '/includes/cache.inc.php';

$user = getFromBitbucket("/2.0/user/");

if ($user && $user->username) {
    $repositories = [];

    $total = 1;
    $page = 1;

    $count = 0;

    $repositories = $cache;

    $projects = ['ARCHIVE', 'BASE', 'BOOK', 'COMPONENTS', 'CUSTOMERS', 'DEMO', 'DOCKER', 'INTERN', 'PUBLIC', 'WINGZZ', 'PACKAGES'];

//    foreach ($projects as $project) {
//        $url = "/2.0/repositories/" . $user->username . "?q=project.key=\"".$project."\"&page=";
//        $page = 1;
//        while ($data = getFromBitbucket($url . $page)) {
//            //echo "Call " . $url . $page."\n";
//            $total = ceil($data->size / $data->pagelen);
//            //$total = 178;
//
//            foreach ((array)$data->values as $repo) {
//                $count++;
//                $newObject = new stdClass();
//                $newObject->name = $repo->name;
//                $newObject->scm = $repo->name;
//
//                $repositories[$repo->scm][] = (object)$newObject;
//            }
//            if ($page >= $total) {
//                break;
//            }
//            $page++;
//            //break;
//        }
//
//    }
//
//    //echo (count($repositories['hg'])+count($repositories['git']));
//    //exit;
//    var_export($repositories);
//    exit;
    //Debugh

}

$repos = [];

foreach ($cache['git'] as $repository) {
    $repos['name'][] = $repository['name'];
    $repos['full_name'][] = $repository['full_name'];
}

//    var_export($repositories);
//    exit;
    //var_dump($cache['hg']);
    //exit;

$mapping = [];
$gitslugs = [];
foreach ($cache['hg'] as $index => $repository) {
    $mapping['hg'][$index] = $repository;
    if (strpos($repository['name'], 'HG') !== false) {
        if (in_array(str_replace(" HG", "", $repository['name']), $repos['name']) || in_array(str_replace(" HG", " v1", $repository['name']), $repos['name'])) {
            $idx = array_search(str_replace(" HG", "", $repository['name']), $repos['name']);

            if (!$idx) {
                $idx = array_search(str_replace(" HG", " v1", $repository['name']), $repos['name']);
            }

            if ($idx) {
                $mapping['git'][$index] = ['name' => $repos['name'][$idx], 'full_name' => $repos['full_name'][$idx]];
                $gitslugs[] = $repos['full_name'][$idx];
            }
        } else {
            $mapping['git'][$index] = null;
        }
    } else {
        if (in_array('Project ' . $repository['name'], $repos['name'])) {
            $idx = array_search('Project ' . $repository['name'], $repos['name']);

            if ($idx) {
                $mapping['git'][$index] = ['name' => $repos['name'][$idx], 'full_name' => $repos['full_name'][$idx]];
                $gitslugs[] = $repos['full_name'][$idx];
            }
        } else if (strpos($repository['name'], 'WiNGZZ') !== false) {
            if (in_array(str_replace("WiNGZZ / Packages", "Package", $repository['name']), $repos['name']) || in_array(str_replace("WiNGZZ / Packages", "Packages", $repository['name']), $repos['name']) || in_array(str_replace("WiNGZZ / Bundle", "Bundle", $repository['name']), $repos['name'])) {
                $idx = array_search(str_replace("WiNGZZ / Packages", "Package", $repository['name']), $repos['name']);

                if (!$idx) {
                    $idx = array_search(str_replace("WiNGZZ / Packages", "Packages", $repository['name']), $repos['name']);
                }

                if (!$idx) {
                    $idx = array_search(str_replace("WiNGZZ / Bundle", "Bundle", $repository['name']), $repos['name']);
                }

                if ($idx) {
                    $mapping['git'][$index] = ['name' => $repos['name'][$idx], 'full_name' => $repos['full_name'][$idx]];
                    $gitslugs[] = $repos['full_name'][$idx];
                }
            }
        } else {
            $mapping['git'][$index] = null;
        }
    }
}

echo '<table border="1">';
foreach ($mapping['hg'] as $index => $repository) {
    echo '<tr>';
    echo '<td><a href="https://bitbucket.org/'.$repository["full_name"].'" target="_blank">' . $repository['name'] . '</a></td>';
    if (isset($mapping['git'][$index])) {
        $gitrepository = $mapping['git'][$index];
        echo '<td><a href="https://bitbucket.org/'.$gitrepository["full_name"].'" target="_blank">' . $gitrepository['name'] . '</a></td>';
    } else {
        echo '<td>FOUT</td>';
    }
    echo '</tr>';
}

foreach ($cache['git'] as $index => $gitrepository) {
    if (!in_array($gitrepository['full_name'], $gitslugs)) {
        echo '<tr>';
        echo '<td>&nbsp;</td>';
        echo '<td><a href="https://bitbucket.org/'.$gitrepository["full_name"].'" target="_blank">' . $gitrepository['name'] . '</a></td>';
        echo '</tr>';
    }
}

echo '</table>';
exit;
//Debugh
echo '<pre>';
var_dump($mapping);
echo '</pre>';
die;

    echo '<table border="1">';
    foreach ($cache['hg'] as $repository) {
        echo '<tr>';
        echo '<td><a href="https://bitbucket.org/'.$repository["full_name"].'" target="_blank">' . $repository['name'] . '</a></td>';

        if (strpos($repository['name'], 'HG') !== false) {
            if (in_array(str_replace(" HG", "", $repository['name']), $repos) || in_array(str_replace(" HG", " v1", $repository['name']), $repos)) {
                $name = str_replace(" HG", "", $repository['name']);
                $full_name = str_replace(" -hg", "", $repository['full_name']);
                echo '<td><a href="https://bitbucket.org/'.$full_name . '">'.$name.'</a></td>';
            } else {
                echo '<td>FOUT</td>';
            }
        } else {
            if (in_array('Project ' . $repository['name'], $repos)) {
                echo '<td>'.'Project ' . $repository['name'].'</td>';
            } else if (strpos($repository['name'], 'WiNGZZ') !== false) {
                if (in_array(str_replace("WiNGZZ / Packages", "Package", $repository['name']), $repos) || in_array(str_replace("WiNGZZ / Packages", "Packages", $repository['name']), $repos)) {
                    echo '<td>'.str_replace("WiNGZZ / Packages", "Package", $repository['name']).'</td>';
                }
            } else {
                echo '<td>FOUT</td>';
            }
        }
        echo '</tr>';
    }
    echo '</table>';
//}