#!/usr/bin/php -q
<?php

require_once __DIR__."/vendor/autoload.php";

require_once dirname(__FILE__) . '/configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/includes/bitbucket.inc.php';

$root = getcwd();

$queue = glob($root."/data/queue/*");

foreach ($queue as $queueItem) {
    $repo_slug = basename($queueItem);
    $repo_fullname = file_get_contents($queueItem);

    if (!file_exists($root . "/data/git/" . $repo_slug)
        || preg_match("/-hg$/", $repo_slug)
    ) {
        continue;
    }
    echo "Push repo ".$repo_slug;

    $oauth_params = array(
        'client_id' => 'dWBWrS2gzHW5mGnu6S',
        'client_secret' => 'rg4SGqH4MyKhkRsdQYsqduekDcsA7nEn'
    );

    $account_name = "studioibizz";

    $repository = new \Bitbucket\API\Repositories\Repository();
    $repository->getClient()->addListener(
        new \Bitbucket\API\Http\Listener\OAuth2Listener($oauth_params)
    );

    $rawResponse = $repository->get($account_name, $repo_slug);

    $response = json_decode($rawResponse->getContent());

    //$tmp = $repository->update($account_name, $repo_slug, array(
    //   'name' => $repo_fullname . " HG"
    //));

    $tmp = $repository->create($account_name, "project-".$repo_slug, array(
        'scm' => 'git',
        //'project' => ['key' => 'PUBLIC'],
        'name' => "Project ".$repo_fullname,
        'description' => $response->description,
        'language' => $response->language,
        'is_private' => $response->is_private,
        'fork_policy' => $response->fork_policy,
    ));

    chdir($root."/data/git/".$repo_slug);
    $repo_slug = "project-".$repo_slug;
    `git remote add origin git@bitbucket.org:studioibizz/$repo_slug.git`;
    `git push --all origin`;
    `git push origin --tags`;


//    `curl --request PUT --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug-hg/studioibizz/archive --data read`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/archive`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug-hg/studioibizz/backend-developers`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug-hg/studioibizz/frontend-developers`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug-hg/studioibizz/lead-developer`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug-hg/studioibizz/administrators`;

    chdir($root);

    //rename($root."/data/queue/".$repo_slug, $root."/data/done/".$repo_slug);
}
