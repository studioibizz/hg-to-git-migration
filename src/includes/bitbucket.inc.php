<?php

function getFromBitbucket($url) {
    global $config;
    $url = "https://".$config['username'].":".$config['apikey']."@bitbucket.org/!api/".trim($url,"/");

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);
    curl_close($ch);

    $string = json_decode($output);
    if(json_last_error() == JSON_ERROR_NONE) {
        return $string;
    }
    return null;
}