#!/usr/bin/php -q
<?php

/**
 * CONVERT HG HASHES IN COMPOSER.JSON AND COMPOSER.LOCK TO GIT HASHES
 */

$root = getcwd();

$repoqueue = glob("../data/queue/*");
$packagequeue = glob("data/queue/*");
$translationqueue = glob("data/converted/*");

$translateTable = [];

// Build translation table
foreach ($translationqueue as $translation) {
    $pathinfo = pathinfo($translation);
    $repo_slug = $pathinfo["filename"];
    $translationString = file_get_contents("data/converted/".$repo_slug.".txt");
    $translationArray = explode("\n", $translationString);

    foreach ($translationArray as $index => $item) {
        $translationArray2 = explode(":", $item);
        if (!empty($translationArray2[0])) {
            $translateTable[$repo_slug][$translationArray2[0]] = $translationArray2[1];
        }
    }
}

// For each repo in queue
foreach ($repoqueue as $queueItem) {
    $repo_slug = basename($queueItem);

    // If non-existant in data/git folder, continue
    if (!file_exists("../data/git/" . $repo_slug)
        || preg_match("/-hg$/", $repo_slug)
    ) {
        continue;
    }

    $repo_fullname = file_get_contents($queueItem);
    chdir("../data/git/".$repo_slug);

    $output = `git branch | cut -c 3-`;
    $branches = explode("\n", $output);
    $branches = array_filter($branches);

    foreach ($branches as $branch) {
        `git checkout -f -q {$branch}`;

        $root_folder = "";
        if (file_exists("src")) {
            $root_folder = "src/";
        }

        if (!file_exists($root_folder . "composer.lock") && !file_exists($root_folder . "composer.lock")) {
            continue;
        }
        // Read the lockfile and decode it into an array, which we can work with
        $lockString = file_get_contents($root_folder . "composer.lock");
        $lockArray = json_decode($lockString);

        $jsonString = file_get_contents($root_folder . "composer.json");
        $jsonArray = json_decode($jsonString);

        // For each package in queue
        foreach ($packagequeue as $packageItem) {
            $full_package_slug = basename($packageItem);

            chdir($root);
            // If non-existant in data/git folder, continue
            if (!file_exists("data/git/" . $full_package_slug)
                || preg_match("/-hg$/", $full_package_slug)
            ) {
                continue;
            }

            preg_match("/component-(.*)/", basename($packageItem), $matches);

            $package_slug = $matches[1];

            $package_fullname = file_get_contents($packageItem);

            $found = [];
            $lookup = [];

            foreach ($lockArray->packages as $index => $package) {
                $packagename = $package->name;
                if ($package->name == "components/jqueryui-touch-punch") {
                    $packagename = "components/jquery-ui-touch-punch";
                }

                if ($packagename == "components/" . $package_slug) {
                    if (!isset($translateTable[$full_package_slug])) {
                        continue;
                    }

                    $hg_hashes = array_keys($translateTable[$full_package_slug]);
                    echo $package->name."\n";
                    //var_dump($hg_hashes);
                    //var_dump($package);
                    $found[$packagename] = false;
                    foreach ($hg_hashes as $hghash) {
                        echo $hghash . " => " . $package->source->reference."\n";
                        if (preg_match("/^".$package->source->reference."/", $hghash)) {
                            $found[$packagename] = true;
                            $lookup[$packagename] = $hghash;
                            break;
                        }
                    }

                    if ($packagename == "components/" . $package_slug && $found[$packagename]) {
                        $package->source->type = "git";
                        $package->source->url = "ssh://git@bitbucket.org/studioibizz/" . str_replace("component-","components-",$full_package_slug);

                        $hg_reference = $package->source->reference;
                        $package->source->reference = $translateTable[$full_package_slug][$lookup[$packagename]];
                    }
                }
            }
        }

        if (isset($lockArray)) {
            $lockString = json_encode($lockArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            file_put_contents("../data/git/" . $repo_slug . "/" . $root_folder . "composer.lock", $lockString);

            chdir("../data/git/".$repo_slug);
            $composer_pattern = $root_folder."composer.lock";
            `git add $composer_pattern`;

            `git config user.email "hans@ibizz.nl"`;
            `git config user.name "Migration Process"`;
            echo "Commit to repo ".$repo_slug."\n";
            `git commit -m "hg to git migration components"`;
        }
    }

    `git push --all origin`;
    chdir($root);
}

exit;
