#!/bin/bash

# CLONE FROM HG, CREATE EMPTY GIT REPO AND PUSH FROM HG TO GIT

if [ $# -eq 0 ]
  then
    echo "Pass pattern as argument"
    exit 1
fi

root=$(pwd)
queue_dir="$root/data/queue"
done_dir="$root/data/done"
hg_dir="$root/data/hg"
git_dir="$root/data/git"

batch_size=5
index=1

pattern=$1

for file in $queue_dir/*
do
    if [[ -f $file ]]; then
        repo_slug=$(basename $file)

        if [[ ! $repo_slug =~ $pattern ]]; then
          continue
        fi

        cd $hg_dir

        if [ -d $repo_slug ]; then
            echo "=== Update $repo_slug ==="
            cd $repo_slug
            hg pull
            hg update --clean
            cd $hg_dir
        else
            echo "=== Clone $repo_slug ==="
            hg clone ssh://hg@bitbucket.org/studioibizz/$repo_slug
        fi

        if [[ ! -d $repo_slug ]]; then
            echo "Cloning of repo is failed"
            continue
        fi

        echo "=== Remove Git repo ==="
        rm -rf "$git_dir/$repo_slug"

        echo "=== Create backup for $repo_slug ==="
        tar -czf $repo_slug.tar.gz -C $hg_dir $repo_slug

        echo "=== Bookmark branches of $repo_slug ==="
        echo -e "\tGo to repo $repo_slug"
        cd $repo_slug

        OLDIFS=$IFS
        IFS=$'\n'
        for branch in $(hg branches -T "{branch}\n")
        do
          if [[ $branch =~ ":" || $branch == "(inactive)" ]]
          then
            continue
          fi

          echo -e "\tUpdate to brach $branch"
          hg update $branch --clean

          actualbranch=$(hg branch)
          newbranch="${actualbranch// /-}"
          newbranch="${newbranch//#/}"
          echo -e "\tReplace $actualbranch to $newbranch"
          echo -e "\tCreate bookmark 'git-$newbranch'"
          hg bookmark "git-$newbranch"
        done
        IFS=$OLDIFS

        echo "=== Create git folder $repo_slug  and initialize ==="
        mkdir $git_dir/$repo_slug
        cd $git_dir/$repo_slug
        git init

        echo "=== Push from hg repo $repo_slug to git repo $repo_slug ==="
        cd $hg_dir/$repo_slug
        hg push $git_dir/$repo_slug

        cd $git_dir/$repo_slug

        for branch in $(git branch | cut -c 3-)
        do
          if [[ $branch =~ ":" || $branch == "(inactive)" ]]
          then
            continue
          fi
          empty=""
          new_branch="${branch/git-/}"
          git checkout -f -q $branch
          echo "=== Rename branch from $branch to $new_branch ==="
          git branch -m $new_branch
          echo "=== Checkout $new_branch ==="
          git checkout -f -q $new_branch
        done

        echo "=== Remove hg folder $repo_slug ==="
#        rm -rf $hg_dir/$repo_slug
    fi

#    if [ $index -eq $batch_size ]
#    then
#      break
#    fi

    index=$(($index+1))
done