#!/usr/bin/php -q
<?php

/**
 * MIGRATE HG PACKAGES TO GIT PACKAGES
 */

require_once __DIR__."/../vendor/autoload.php";

require_once dirname(__FILE__) . '/../configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/../includes/bitbucket.inc.php';

$root = getcwd();

$repoqueue = glob("../data/queue/*");
$packagequeue = glob("data/queue/*");

foreach ($packagequeue as $queueItem) {
    $repo_slug = basename($queueItem);
    $git_repo_slug = str_replace("wingzz-packages-", "package-", $repo_slug);
    $git_repo_slug = str_replace("wingzz-bundle-", "bundle-", $git_repo_slug);

    if ($repo_slug == $git_repo_slug) {
        continue;
    }

    $repo_fullname = file_get_contents($queueItem);
    $git_repo_fullname = trim(str_replace("WiNGZZ / Packages", "Package", $repo_fullname));
    if ($repo_fullname == $git_repo_fullname) {
        continue;
    }

    if (!file_exists($root . "/data/git/" . $repo_slug)
        || preg_match("/-hg$/", $repo_slug)
    ) {
        continue;
    }
    echo "Create repo ".$git_repo_slug;

    $oauth_params = array(
        'client_id' => 'dWBWrS2gzHW5mGnu6S',
        'client_secret' => 'rg4SGqH4MyKhkRsdQYsqduekDcsA7nEn'
    );

    $account_name = "studioibizz";

    $repository = new \Bitbucket\API\Repositories\Repository();
    $repository->getClient()->addListener(
        new \Bitbucket\API\Http\Listener\OAuth2Listener($oauth_params)
    );

    $rawResponse = $repository->get($account_name, $repo_slug);

    $response = json_decode($rawResponse->getContent());

//    $repository->update($account_name, $repo_slug, array(
//        'name' => $repo_fullname . " HG"
//    ));

    $tmp = $repository->create($account_name, $git_repo_slug, array(
        'scm' => 'git',
        'name' => $git_repo_fullname,
        'description' => $response->description,
        'language' => $response->language,
        'project' => [
          "key" => "PACKAGES"
        ],
        'is_private' => $response->is_private,
        'fork_policy' => $response->fork_policy,
    ));
    echo ": Created".PHP_EOL;

    echo "Push repo ".$repo_slug.PHP_EOL;
    chdir($root."/data/git/".$repo_slug);
    `git remote remove origin >/dev/null 2>&1`;
    `git remote add origin git@bitbucket.org:studioibizz/$git_repo_slug.git`;
    `git push --all origin`;
    `git push origin --tags`;


//    `curl --request PUT --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/archive --data read`;
//    `curl --request PUT --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/backend-developers --data read`;
//    `curl --request PUT --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/frontend-developers --data read`;
//    `curl --request PUT --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/lead-developer --data read`;

// Commands when migration is succesful done:
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/archive`;
//    `curl --request DELETE --user studioibizz:4Z39dkjHfwzIwJOzdlLQb7gWiwhwfmSf https://api.bitbucket.org/1.0/group-privileges/studioibizz/$repo_slug/studioibizz/administrators`;

    chdir($root);
}
