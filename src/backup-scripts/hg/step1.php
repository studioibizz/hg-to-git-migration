<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__."/../../vendor/autoload.php";

require_once dirname(__FILE__) . '/../../configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/../../includes/bitbucket.inc.php';

foreach ([
    "data/queue",
    "data/done",
    "data/hg",
] as $dir) {
    @mkdir($dir, 0755, true);
}

$oauth_params = array(
    'client_id' => 'dWBWrS2gzHW5mGnu6S',
    'client_secret' => 'rg4SGqH4MyKhkRsdQYsqduekDcsA7nEn'
);

$account_name = "studioibizz";

$repositories = new \Bitbucket\API\Repositories();
$repositories->getClient()->addListener(
    new \Bitbucket\API\Http\Listener\OAuth2Listener($oauth_params)
);

$page = new \Bitbucket\API\Http\Response\Pager($repositories->getClient(), $repositories->all($account_name));

$rawResponse = $page->getCurrent();

$response = json_decode($rawResponse->getContent());
$data = $response->values;

$repositories = [];

$index = 0;
$count = 0;

while ($rawResponse = $page->hasNext()) {
    if ($count == 0) {
        $rawResponse = $page->getCurrent();
    } else {
        $rawResponse = $page->fetchNext();
    }

    $response = json_decode($rawResponse->getContent());
    $data = $response->values;

    foreach ((array)$data as $repo) {
        if ($repo->scm != 'hg') continue;
        $count += 1;

        file_put_contents("data/queue/" . $repo->slug, $repo->name);
    }
    $index++;

}
printf('%d hg repos queued', $count);
