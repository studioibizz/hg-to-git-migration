#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Pass pattern as argument"
    exit 1
fi

root=$(pwd)
queue_dir="$root/data/queue"
done_dir="$root/data/done"
hg_dir="$root/data/hg"

pattern=$1

for file in $queue_dir/*
do
    if [[ -f $file ]]; then
        repo_slug=$(basename $file)

        if [[ ! $repo_slug =~ $pattern ]]; then
          continue
        fi

        cd $hg_dir

        if [ -d $repo_slug ]; then
            echo "=== Update $repo_slug ==="
            cd $repo_slug
            hg pull
            hg update --clean
            cd $hg_dir
        else
            echo "=== Clone $repo_slug ==="
            hg clone ssh://hg@bitbucket.org/studioibizz/$repo_slug
        fi

        if [[ ! -d $repo_slug ]]; then
            echo "Cloning of repo is failed"
            continue
        fi

        echo "=== Create backup for $repo_slug ==="
        tar -czf $repo_slug.tar.gz -C $hg_dir $repo_slug

        echo "=== Remove hg folder $repo_slug ==="
        rm -rf $hg_dir/$repo_slug

        mv $queue_dir/$(basename $file) $done_dir/$(basename $file)
    fi
done