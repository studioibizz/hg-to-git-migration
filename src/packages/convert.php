#!/usr/bin/php -q
<?php

/**
 * CONVERT HG HASHES IN COMPOSER.JSON AND COMPOSER.LOCK TO GIT HASHES
 */

$root = getcwd();

$repoqueue = glob("../data/queue/*");
$packagequeue = glob("data/queue/*");
$translationqueue = glob("data/converted/*");

$translateTable = [];

// Build translation table
foreach ($translationqueue as $translation) {
    $pathinfo = pathinfo($translation);
    $repo_slug = $pathinfo["filename"];
    $translationString = file_get_contents("data/converted/".$repo_slug.".txt");
    $translationArray = explode("\n", $translationString);

    foreach ($translationArray as $index => $item) {
        $translationArray2 = explode(":", $item);
        if (!empty($translationArray2[0])) {
            $translateTable[$repo_slug][$translationArray2[0]] = $translationArray2[1];
        }
    }
}

// For each repo in queue
foreach ($repoqueue as $queueItem) {
    $repo_slug = basename($queueItem);

    // If non-existant in data/git folder, continue
    if (!file_exists("../data/git/" . $repo_slug)
        || preg_match("/-hg$/", $repo_slug)
    ) {
        continue;
    }

    $repo_fullname = file_get_contents($queueItem);
    chdir("../data/git/".$repo_slug);

    $output = `git branch | cut -c 3-`;
    $branches = explode("\n", $output);
    $branches = array_filter($branches);

    foreach ($branches as $branch) {
        `git checkout -f -q {$branch}`;

        $root_folder = "";
        if (file_exists("src")) {
            $root_folder = "src/";
        }

        if (!file_exists($root_folder . "composer.lock") && !file_exists($root_folder . "composer.lock")) {
            continue;
        }
        // Read the lockfile and decode it into an array, which we can work with
        $lockString = file_get_contents($root_folder . "composer.lock");
        $lockArray = json_decode($lockString);

        $jsonString = file_get_contents($root_folder . "composer.json");
        $jsonArray = json_decode($jsonString);

        // For each package in queue
        foreach ($packagequeue as $packageItem) {
            $full_package_slug = basename($packageItem);

            chdir($root);
            // If non-existant in data/git folder, continue
            if (!file_exists("data/git/" . $full_package_slug)
                || preg_match("/-hg$/", $full_package_slug)
            ) {
                continue;
            }

            if (basename($packageItem) == "hostercheck") continue;

            preg_match("/wingzz-(bundle|packages)-(.*)/", basename($packageItem), $matches);

            $package_slug = $matches[2];

            $package_fullname = file_get_contents($packageItem);

            $found = [];
            $lookup = [];

            foreach ($lockArray->packages as $index => $package) {
                if ($package->name == "studioibizz/linkchecker" && $package_slug == "link-checker") {
                    $package->name = "studioibizz/link-checker";
                }

                if ($package->name == "studioibizz/filemanagement" && $package_slug == "file-management") {
                    $package->name = "studioibizz/file-management";
                }

                if ($package->name == "studioibizz/socialmedia" && $package_slug == "social-media") {
                    $package->name = "studioibizz/social-media";
                }

                if ($package->name == "studioibizz/restservice" && $package_slug == "rest-service") {
                    $package->name = "studioibizz/rest-service";
                }

                if ($package->name == "studioibizz/soapgenerator" && $package_slug == "soap-generator") {
                    $package->name = "studioibizz/soap-generator";
                }

                if ($package_slug == "minimal") {
                    $package_slug = "bundle-minimal";
                }

                if ($package_slug == "website") {
                    $package_slug = "bundle-website";
                }

                if ($package_slug == "webshop") {
                    $package_slug = "bundle-webshop";
                }

                if ($package->name == "studioibizz/" . $package_slug) {
                    if (!isset($translateTable[$full_package_slug])) {
                        continue;
                    }

                    $hg_hashes = array_keys($translateTable[$full_package_slug]);
                    echo $package->name."\n";
                    //var_dump($hg_hashes);
                    //var_dump($package);
                    $found[$package->name] = false;
                    foreach ($hg_hashes as $hghash) {
                        echo $hghash . " => " . $package->source->reference."\n";
                        if (preg_match("/^".$package->source->reference."/", $hghash)) {
                            $found[$package->name] = true;
                            $lookup[$package->name] = $hghash;
                            break;
                        }
                    }

                    if ($package->name == "studioibizz/" . $package_slug && $found[$package->name]) {
                        $package->source->type = "git";
                        $package->source->url = "ssh://git@bitbucket.org/studioibizz/" . str_replace("wingzz-packages-","package-",$full_package_slug);

                        $hg_reference = $package->source->reference;
                        $package->source->reference = $translateTable[$full_package_slug][$lookup[$package->name]];

                        if ($package->name == "studioibizz/link-checker" && $package_slug == "link-checker") {
                            echo "FOUND!!!";
                            $package->name = "studioibizz/linkchecker";
                            $lockArray->packages[$index]->name = "studioibizz/linkchecker";
                            print_r($lockArray->packages[$index]);
                        }

                        if ($package->name == "studioibizz/file-management" && $package_slug == "file-management") {
                            $package->name = "studioibizz/filemanagement";
                            $lockArray->packages[$index]->name = "studioibizz/filemanagement";
                        }

                        if ($package->name == "studioibizz/social-media" && $package_slug == "social-media") {
                            $package->name = "studioibizz/socialmedia";
                            $lockArray->packages[$index]->name = "studioibizz/socialmedia";
                        }

                        if ($package->name == "studioibizz/rest-service" && $package_slug == "rest-service") {
                            $package->name = "studioibizz/restservice";
                            $lockArray->packages[$index]->name = "studioibizz/restservice";
                        }

                        if ($package->name == "studioibizz/soap-generator" && $package_slug == "soap-generator") {
                            $package->name = "studioibizz/soapgenerator";
                            $lockArray->packages[$index]->name = "studioibizz/soapgenerator";
                        }

                    }
                }
            }

            foreach ($jsonArray->require as $repo => $version) {
                if ($repo == "studioibizz/" . $package_slug) {
                    if (isset($found[$repo]) && $found[$repo]) {
                        $git_reference = $translateTable[$full_package_slug][$lookup[$repo]];
                        echo "git: ".$git_reference."\n";
                        $jsonArray->require->$repo = str_replace("#" . $hg_reference, "#" . $git_reference, $jsonArray->require->$repo);
                    }
                }
            }
        }

        if (isset($jsonArray)) {
            foreach ((array)$jsonArray->repositories as $repository) {
                if ($repository->url == "https://developers.ibizz.nl/packages/" || $repository->url == "https://developers.ibizz.nl/packages") {
                    $repository->url = "https://developers.ibizz.nl/repos/";
                }
            }

            $lockString = json_encode($lockArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            file_put_contents("../data/git/" . $repo_slug . "/" . $root_folder . "composer.lock", $lockString);

            $jsonString = json_encode($jsonArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            file_put_contents("../data/git/" . $repo_slug . "/". $root_folder . "composer.json", $jsonString);

            chdir("../data/git/".$repo_slug);
            $composer_pattern = $root_folder."composer.*";
            `git add $composer_pattern`;
            `cp ../../../../.gitattributes .`;

            `git add .gitattributes`;
            `git config user.email "hans@ibizz.nl"`;
            `git config user.name "Migration Process"`;
            echo "Commit to repo ".$repo_slug."\n";
            `git commit -m "hg to git migration"`;
        }
    }

    `git push --all origin`;
    chdir($root);
}

exit;
