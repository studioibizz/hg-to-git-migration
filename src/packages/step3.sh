#!/bin/bash

# CREATE A FILE WITH THE COMBINATION DATETIME AND HASH

if [ $# -eq 0 ]
  then
    echo "Pass pattern as argument"
    exit 1
fi

root=$(pwd)
queue_dir="$root/data/queue"
done_dir="$root/data/done"
hg_dir="$root/data/hg"
git_dir="$root/data/git"

batch_size=5
index=1

pattern=$1

for file in $queue_dir/*
do
    if [[ -f $file ]]; then
        repo_slug=$(basename $file)

        if [[ ! $repo_slug =~ $pattern ]]; then
          continue
        fi

        echo "=== Create file with all hg commit hashes ==="
        cd $hg_dir/$repo_slug
        hg log --template '{date|isodatesec}~{node}\n' > $repo_slug.txt

        echo "=== Create file with all git commit hashes ==="
        cd $git_dir/$repo_slug
        git log --pretty="format:%ai~%H" --all > $repo_slug.txt
    fi
done