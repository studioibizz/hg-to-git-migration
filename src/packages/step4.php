#!/usr/bin/php -q
<?php

/**
 * COMBINE THE HASHES FROM HG WITH GIT
 */

require_once __DIR__."/../vendor/autoload.php";

require_once dirname(__FILE__) . '/../configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/../includes/bitbucket.inc.php';

$root = getcwd();

$queue = glob($root."/data/queue/*");

foreach ($queue as $queueItem) {
    $repo_slug = basename($queueItem);
    $repo_fullname = file_get_contents($queueItem);

    if (!file_exists($root . "/data/git/" . $repo_slug)
        || preg_match("/-hg$/", $repo_slug)
    ) {
        continue;
    }

    $hgString = file_get_contents("data/hg/".$repo_slug."/".$repo_slug.".txt");
    $hgArray = array_filter(explode("\n", $hgString));

    $hgtable = [];
    foreach ($hgArray as $hgItem) {
        $itemArray = explode("~", $hgItem);

        if (isset($hgtable[$itemArray[0]])) {
            $hgtable[$itemArray[0]."-1"] = $itemArray[1];
        } else {
            $hgtable[$itemArray[0]] = $itemArray[1];
        }
    }

    $gitString = file_get_contents("data/git/".$repo_slug."/".$repo_slug.".txt");
    $gitArray = array_filter(explode("\n", $gitString));

    $gittable = [];
    foreach ($gitArray as $gitItem) {
        $itemArray = explode("~", $gitItem);

        if (isset($gittable[$itemArray[0]])) {
            $gittable[$itemArray[0]."-1"] = $itemArray[1];
        } else {
            $gittable[$itemArray[0]] = $itemArray[1];
        }
    }

    $translateTable = [];

    echo "Commit translation table\n";
    foreach ($hgtable as $timestamp => $item) {
        if (empty($item)) continue;
        $translateTable[$item] = (isset($gittable[$timestamp]) ? $gittable[$timestamp] : "*");

        echo sprintf("%s | %s | %s\n", $timestamp, $item, $translateTable[$item]);
    }

    $content = "";
    foreach ($translateTable as $hghash => $githash) {
        $content .= sprintf("%s:%s\n", $hghash, $githash);
    }

    file_put_contents("data/converted/".$repo_slug.".txt", $content);
}
