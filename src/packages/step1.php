#!/usr/bin/php -q
<?php

/**
 * BUILD A QUEUE WITH FILES FROM PACKAGES
 */

require_once dirname(__FILE__) . '/../configs/bitbucket.config.php';
require_once dirname(__FILE__) . '/../includes/bitbucket.inc.php';

echo "* We try to get a connection with Bitbucket";
$time = time();
$user = getFromBitbucket("/2.0/user/");

foreach ([
    "data/queue",
    "data/done",
    "data/hg",
    "data/git",
    "data/converted",
] as $dir) {
    @mkdir($dir, 0755, true);
}

if ($user && $user->username) {
    echo ": Hello " . $user->display_name . " and welcome!(" . (time() - $time) . "s)" . PHP_EOL;
    echo "* Get WiNGZZ repositories from Bitbucket";
    $time = time();

    $repositories = [];

    $total = 1;
    $page = 1;

    $count = 0;

    $url = "/2.0/repositories/" . $user->username . "?q=project.key=\"WINGZZ\"&page=";
    while ($data = getFromBitbucket($url . $page)) {
        echo "Call " . $url . $page."\n";
        $total = ceil($data->size / $data->pagelen);

        foreach ((array)$data->values as $repo) {
            if (!$repo->slug || $repo->scm != "hg") {
                continue;
            }

            $count++;
            $repositories[] = $repo;

            file_put_contents("data/queue/" . $repo->slug, $repo->name);

        }
        if ($page >= $total) {
            break;
        }
        $page++;
    }
}