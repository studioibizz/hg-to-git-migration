# Stappenplan conversie hg klant repo naar git repo inclusief package repo's
---    
1. Maak voor alle Hg package repo's een queue bestand aan. Het queue bestand wordt opgeslagen in "packages/data/queue" 
met als bestandsnaam de repo slug en als inhoud de repo naam. Bestand: packages/step1.php

2. Middels een argument kan bepaald worden welke repo's geprocessed worden. Dit werkt ook met reguliere expressies.
    Bestand: packages/step2.sh
    1. Hg repo wordt gecloned of gepulled en geüpdatet (indien de repo al aanwezig is) in packages/data/hg.
    2. Er wordt een backup gemaakt van de hg repo in een tar.gz bestand.
    3. Er wordt een bookmark per actieve branch aangemaakt in de hg repo, dit is later nodig voor het bepalen 
    van branches in git. De bookmarks zijn in het formaat: git-[repo slug].
    4. Maak een folder aan met als naam de repo slug onder packages/data/git.
    5. Initialiseer de lege git repo met git init.
    6. Push vanaf de hg repo naar de git repo. Dit gebeurd middels een speciaal stukje software die de conversie doet.
    7. Ga naar packages/data/git/[repo slug] en loop door alle actieve branches. Alle branches beginnen met git-. 
    Hernoem ze weer naar de oorspronkelijke naam zonder deze prefix.
    8. Verwijder Hg folder.

3. Middels een argument kan bepaald worden welke repo's geprocessed worden. Dit werkt ook met reguliere expressies.
    Bestand: packages/step3.sh
    1. Ga naar packages/data/hg/[repo slug] en voer hier het log commando uit met specifiek format. Sla het resultaat op
    in [repo slug].txt.
    2. Ga naar packages/data/git/[repo slug] en voer hier het log commando uit met specifiek format. Sla het resultaat 
    op in [repo slug].txt

4. Repo's waar een folder voor aanwezig is in data/git worden geprocessed.
    Bestand: packages/step4.php
    1. Ga naar packages/data/hg/[repo slug] en lees deze in in een array in het formaat array[timestamp] = [hg hash]
    2. Ga naar packages/data/git/[repo slug] en lees deze in in een array in het formaat array[timestamp] = [git hash]
    3. Bouw een translation array op met als formaat array[hg hash] = [git hash]
    4. Sla deze translation array op in packages/data/converted/[repo slug].txt

5. Repo's waar een folder voor aanwezig is in packages/data/git worden geprocessed.
    Bestand: packages/step5.php   
    1. Hernoem de Hg repo op bitbucket door HG achter de repo naam te plakken.
    2. Creëer Git repo op bitbucket met als naam de repo slug.
    3. Stel rechten in voor de remote hg repo    
    4. Push lokale git repo naar remote git repo.

6. Maak voor alle Hg klant repo's een queue bestand aan. Het queue bestand wordt opgeslagen in "data/queue" met als 
bestandsnaam de repo slug en als inhoud de repo naam. Bestand: step1.php

7. Middels een argument kan bepaald worden welke repo's geprocessed worden. Dit werkt ook met reguliere expressies.
    Bestand: step2.sh
    1. Hg repo wordt gecloned of gepulled en geüpdatet (indien de repo al aanwezig is) in data/hg.
    2. Er wordt een backup gemaakt van de hg repo in een tar.gz bestand.
    3. Er wordt een bookmark per actieve branch aangemaakt in de hg repo, dit is later nodig voor het bepalen 
    van branches in git. De bookmarks zijn in het formaat: git-[repo slug].
    4. Maak een folder aan met als naam de repo slug onder data/git.
    5. Initialiseer de lege git repo met git init.
    6. Push vanaf de hg repo naar de git repo. Dit gebeurd middels een speciaal stukje software die de conversie doet.
    7. Ga naar data/git/[repo slug] en loop door alle actieve branches. Alle branches beginnen met git-. Hernoem ze weer
    naar de oorspronkelijke naam zonder deze prefix.
    8. Verwijder Hg folder.

8.  Repo's waar een folder voor aanwezig is in data/git worden geprocessed.
    Bestand: step3.php
    1. Hernoem de Hg repo op bitbucket door HG achter de repo naam te plakken.
    2. Creëer Git repo op bitbucket met als naam de repo slug.
    3. Push lokale git repo naar remote git repo.
    4. Stel rechten in voor de remote hg repo
    5. Verplaats queue bestand van data/queue naar data/done     

9. Repo's waar een folder voor aanwezig is in packages/data/git worden geprocessed.
    Bestand: packages/convert.php   
    1. Lees composer.lock in en zoek alle entries van studioibizz packages op. Lees de hash uit (hg hash). Zoek hier de 
    git hash bij en vervang vervolgens de hg hash door de git hash.
    2. Lees composer.json in en zoek alle entries van studioibizz packages op. Lees de hash uit (hg hash). Zoek hier de 
    git hash bij en vervang vervolgens de hg hash door de git hash.
    3. Commit en push deze 2 bestanden.
    TODOS: 
    - Toevoegen van bitbucket-pipelines.yaml, .gitattributes en eventueel .gitignore
    - Pipelines aanzetten in bitbucket.
    
    
