#!/usr/bin/php -q
<?php

$contentString = file_get_contents("data/hg/oauth/hashes2.txt");

$contentArray = explode("\n", $contentString);

$table = [];
foreach ($contentArray as $contentItem) {
    $itemArray = explode("~", $contentItem);

    $table[$itemArray[0]] = $itemArray[1];
}
