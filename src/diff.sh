#!/bin/bash

# Tool to diff hg repos and migrated git repos

diff -x 'composer.*' -x '.git' -x '.hg' -x '*.tar.gz' -r ./data/hg ./data/git
if [ $? -eq 1 ]
then
   echo "Difference found"
else
  echo "No difference found"
fi